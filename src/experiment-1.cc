#include <iostream>
using namespace std;
#include "lib/logging.h"
#include "lib/strings.h"

int main() {
	treelog::TreeLogNode log("Super log!", "");
	log
		.add(treelog::TreeLogNode("Mary", "FAI")
			.add("subchild", "DUDE")
			.add("subchild", "DUDE")
			.add(treelog::TreeLogNode("subchild5", "DUDE")
				.add("subchild5", "DUDE")
				.add("subchild5", "DUDE")
				.add("subchild5", "DUDE"))
			.add("subchild", "DUDE"))
		.add(treelog::TreeLogNode("Joe", "PASSED")
			.add("Jo", "HEY")
			.add("Jar", "WTF")
			.add("Jose", "YOU")
			.add(treelog::TreeLogNode("Joseph", "PASSED")
				.add("DUPER", "HEY")
				.add("UPER", "WTF")));

	cout << treelog::renderer::InlineBlockTextRenderer(log).str() << endl;
}
