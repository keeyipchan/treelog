#include <cassert>
#include <iostream>
using namespace std;
#include "lib/logging.h"
#include "lib/strings.h"
using namespace treelog;

int main() {
	TreeLogNode* log = new TreeLogNode("html", "");
	log
		->add((new TreeLogNode("head", ""))
			->add((new TreeLogNode("meta", ""))
				->push("content", "viewport-scale=1")->current()->set_prefix("->")->pop())
			->add("title", "Welcome")
			->add((new TreeLogNode("link", ""))
				->push("rel", "stylesheet")->current()->set_prefix("->")->pop()
				->push("href", "jquery.css")->current()->set_prefix("->")->pop()
				->push("style", "display:none")->current()->set_prefix("->")->pop()))
		->add((new TreeLogNode("body", ""))
			->add((new TreeLogNode("a", ""))
				->push("href", "http://www.google.com")->current()->set_prefix("->")->pop())
			->add((new TreeLogNode("div", " == NEW == "))->set_suffix("#main")
				->add((new TreeLogNode("div", ""))->set_suffix("#postlist-container")
					->add("div#postlist-header", "")
					->add("TEXT", "test")
					->add("div#postlist-content", "")
					->add("div#postlist-actions", ""))));

	int count_a = 0, count_b = 0;
	{
		renderer::BlockTextRenderer renderer(log);
		cout << renderer.str() << endl;
		cout << endl;
		count_a = renderer.count_rendered_nodes();
	}
	{
		renderer::InlineBlockTextRenderer renderer(log);
		cout << renderer.str() << endl;
		cout << endl;
		count_b = renderer.count_rendered_nodes();
	}
	assert(count_a == count_b);
	assert(count_a > 1);
	if (false){
		TreeLogNode* inline_render = renderer::debug;
		cout << renderer::BlockTextRenderer(inline_render).str() << endl;
	}
}
