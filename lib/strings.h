#pragma once

#include <string>
#include <sstream>
#include <list>
#include <iomanip>
using std::string;
using std::stringstream;
using std::setfill;
using std::setw;
using std::list;

namespace strings {

	string remove_escape_codes (string s) {
		stringstream stream;
		const char* c = s.c_str();
		int n = 0;
		for (int i = 0; i < s.length();) {
			if (c[i] == 0) {
				break;
			}

			if (c[i] == '\x1b') {
				if (c[i+3] == 'm') {
					i+=4;
				} else {
					i+=5;
				}
				continue;
			}

			stream << c[i];
			i++;
		}

		return stream.str();
	}

	int visible_length(string s) {
		const char* c = s.c_str();
		int n = 0;
		for (int i = 0; i < s.length();) {
			if (c[i] == 0) {
				break;
			}

			if (c[i] == '\x1b') {
				if (c[i+3] == 'm') {
					i+=4;
				} else {
					i+=5;
				}
				continue;
			}

			if (c[i] == '\xe2') {
				i+=3;
				n++;
				continue;
			}

			i++;
			n++;
		}
		return n;
	}

	int max_line_length (list<string>& strings) {
		int max = 0;
		list<string>::iterator it = strings.begin();
		for (; it != strings.end(); it++) {
			if (visible_length(*it) > max)
				max = visible_length(*it);
		}
		return max;
	}

	void prepend_each_line (
		list<string>& source,
		string prepend_a,
		string prepend_b) {
			list<string>::iterator s = source.begin();
			list<string>::iterator r = source.begin();
			for (; s != source.end(); s++) {
				string str;
				if (s == source.begin())
					str = prepend_a + *s;
				else
					str = prepend_b + *s;
				r = s;
				int offset = str.find('\xe2') - 5;
				if (offset >= 0) {
					for (; r != source.begin(); r--) {
						if (r != s && (*r)[offset] == ' ')
							r->replace(offset, 1, ANSI_BLUE "\xe2\x94\x82");
					}
				}
				s->swap(str);
			}
		}


	void append_each_line (
		list<string>& source,
		list<string>& destination,
		int offset,
		string repeat,
		string INDENT) {
			list<string>::iterator s = source.begin();
			list<string>::iterator d = destination.begin();
			for (; s != source.end(); s++) {
				stringstream stream;
				int x = 0;
				int a = d != destination.end() ? visible_length(*d) : 0;
				if (s == source.begin()) {
					while (a + visible_length(stream.str()) -1< offset) {
						stream << repeat;
					}
					stream << *s;
				} else {
					while (a + visible_length(stream.str()) + 1 < offset) {
						stream << ' ';
					}
					stream << *s;
				}

				if (d == destination.end()) {
					destination.push_back(stream.str());
				} else {
					d->append(stream.str());
					d++;
				}
			}
		}
}
