#pragma once

#include <iostream>
#include "lib/terminal.hpp"
using std::endl;
#include <string>
#include <sstream>
#include <list>
#include "lib/strings.h"
using std::string;
using std::list;
using std::stringstream;
using strings::max_line_length;
using strings::append_each_line;
using strings::prepend_each_line;
using strings::remove_escape_codes;

namespace treelog {
	class TreeLogNode {
		public:
			string name, prefix, suffix, value;
			list<TreeLogNode*> children;
			TreeLogNode* parent_node;

			TreeLogNode* current_node;

			TreeLogNode (
				string given_name,
				string given_value) :
				name(given_name),
				value(given_value),
				prefix(""),
				suffix("") {
					current_node = this;
					parent_node = 0;
				}

			TreeLogNode* add (
				string given_name,
				string given_value) {
					TreeLogNode* t = new TreeLogNode(given_name, given_value);
					return add(t);
				}

			TreeLogNode* add (
				TreeLogNode* new_child) {
					new_child->parent_node = this;
					this->children.push_back(new_child);
					return this;
				}

			TreeLogNode* set_suffix (string given_suffix) {
				this->suffix = given_suffix;
				return this;
			}

			TreeLogNode* set_prefix (string given_prefix) {
				this->prefix = given_prefix;
				return this;
			}

			TreeLogNode* push(
				string given_name,
				string given_value) {
					TreeLogNode* t = new TreeLogNode(given_name, given_value);
					return push(t);
				}
			TreeLogNode* push (
				TreeLogNode* new_child) {
					this->current_node->add(new_child);
					this->current_node = new_child;
					return this;
				}
			TreeLogNode* current () {
				return this->current_node;
			}
			TreeLogNode* pop () {
				this->current_node = this->current_node->parent_node;
				if (!this->current_node)
					this->current_node = this;
				if (this->current_node == this->parent_node) {
					this->current_node = this;
					this->parent_node->current_node = this->parent_node;
					return this->parent_node;
				}
				return this;
			}
	};

	namespace renderer {
		TreeLogNode* debug = new TreeLogNode("RENDER_TREE","");
		class AbstractRenderer {
			public:
				int count_rendered_nodes () { return total_nodes_rendered; }

			protected:
				AbstractRenderer (TreeLogNode* given_root)
					: root(given_root) {
					}

				TreeLogNode* root;
				int total_nodes_rendered;
		};

		class TextRenderer : public AbstractRenderer {
			public:
				virtual string str() = 0;
				string INDENT, HOOK,PIPE, BRANCH;

			protected:
				string line (TreeLogNode* node, int step) {
					stringstream s;
					s << ANSI_BLUE << "(" << ANSI_RESET << step << ANSI_BLUE ")" << ANSI_RESET;
					s << ANSI_BG_BLUE
						<< " " << ANSI_WHITE << node->prefix;
					s << ANSI_GREEN << node->name;
					s << ANSI_WHITE << node->suffix << ANSI_FG_RESET;
					if (node->value.length()) {
						s << "="
							<< ANSI_BG_WHITE
							<< ANSI_BLACK << node->value << ANSI_RESET << " ";
					} else {
						s << " "
						<< ANSI_BG_RESET;
					}
					return s.str();
				}

				TextRenderer (TreeLogNode* given_root) :
					AbstractRenderer(given_root),
					INDENT("   "),
					HOOK(string() + ANSI_BLUE + " \xe2\x94\x9c\xe2\x94\x80" + ANSI_RESET),
					PIPE(string() + ANSI_BLUE + " \xe2\x94\x9c\xe2\x94\x80" + ANSI_RESET),
					BRANCH(string() + ANSI_BLUE + "\xe2\x94\x80" + ANSI_RESET) {
					}
		};

		class BlockTextRenderer : public TextRenderer {
			public:
				BlockTextRenderer (TreeLogNode* given_root)
					: TextRenderer(given_root) {
					}

				virtual string str() {
					stringstream s;
					total_nodes_rendered = 1;
					list<string> lines = str(root, "", total_nodes_rendered);

					list<string>::iterator it = lines.begin();
					for (; it != lines.end(); it++)
						s << *it << endl;

					return s.str();
				}

			private:
				list<string> str (
					TreeLogNode* node,
					string prefix,
					int& step) {
						list<string> lines;
						stringstream s;

						s << prefix << line(node, step);
						lines.push_back(s.str());

						list<TreeLogNode*>::iterator it = node->children.begin();

						for (; it != node->children.end(); it++) {
							step++;
							list<string> sub_lines = str(*it, "", step);
							prepend_each_line(sub_lines, prefix + HOOK, prefix + INDENT);

							lines.splice(lines.end(), sub_lines);
						}

						return lines;
					}
		};

		class InlineBlockTextRenderer : public TextRenderer {
			public:
				InlineBlockTextRenderer (TreeLogNode* given_root)
					: TextRenderer(given_root) {
					}

				virtual string str() {
					debug = new TreeLogNode("DEBUG", "InlineBlockTextRenderer");
					stringstream s;
					total_nodes_rendered = 1;
					list<string> lines = str(root, "", total_nodes_rendered);

					list<string>::iterator it = lines.begin();
					for (; it != lines.end(); it++)
						s << *it << endl;

					return s.str();
				}

			private:
				list<string> str (
					TreeLogNode* node,
					string prefix,
					int& step) {
						list<string> lines;
						stringstream s;

						s << prefix << line(node, step);
						lines.push_back(s.str());

						list<TreeLogNode*>::iterator it = node->children.begin();

						it = node->children.begin();
						int offset = 0;
						list<string> child_lines;
						int SPACING = 1;

						int debug_parent_step = step;
						debug->push("parent", remove_escape_codes(line(node,debug_parent_step)));

							for (; it != node->children.end(); it++) {
								step++;
								debug->push("..child..", remove_escape_codes(line(*it,step)));
								{
									debug->push(">>>","");
										list<string> sub_lines = str(*it, "", step);
										list<string>::iterator p = sub_lines.begin();
									debug->pop();

									debug->push("===","");
										for(;p != sub_lines.end();p++)
											debug->current()->add("", remove_escape_codes(*p));
									debug->pop();

									char buf[256];

									TreeLogNode* ptr = *it;
									if (ptr->children.size()) {
										if (it == node->children.begin()) {
											prepend_each_line(sub_lines, prefix + HOOK, prefix + INDENT);
											offset = max_line_length(sub_lines) + SPACING;
											child_lines.splice(child_lines.end(), sub_lines);

											debug->push("Just 1", "")->current()->set_prefix("==")->set_suffix("==");
												sprintf(buf,"%d", offset);
												debug->current()->add("new_offset", buf);
											{
												list<string>::iterator p = child_lines.begin();
												for(;p != child_lines.end();p++)
													debug->current()->add("", remove_escape_codes(*p));
											}
											debug->pop();
										} else {
											int old_offset = offset;
											prepend_each_line(sub_lines, prefix + BRANCH, prefix + INDENT);
											append_each_line(sub_lines, child_lines, offset, BRANCH, INDENT);
											offset = max_line_length(child_lines) + SPACING;

											debug->push("FOLD", "")->current()->set_prefix("==")->set_suffix("==");
												sprintf(buf,"%d", old_offset);
												debug->current()->add("used_offset", buf);
												sprintf(buf,"%d", offset);
												debug->current()->add(" new_offset", buf);
											{
												list<string>::iterator p = child_lines.begin();
												for(;p != child_lines.end();p++)
													debug->current()->add("", remove_escape_codes(*p));
											}
											debug->pop();
										}
									} else {
										prepend_each_line(sub_lines, prefix + PIPE, prefix + INDENT);
										child_lines.splice(child_lines.end(), sub_lines);
										offset = max_line_length(child_lines) + SPACING;

										debug->push("PIPE", "")->current()->set_prefix("==")->set_suffix("==");
											sprintf(buf,"%d", offset);
											debug->current()->add("new_offset", buf);
										{
											list<string>::iterator p = child_lines.begin();
											for(;p != child_lines.end();p++)
												debug->current()->add("", remove_escape_codes(*p));
										}
										debug->pop();
									}
								}
								debug->pop();
							}

						debug->pop();

						lines.splice(lines.end(), child_lines);

						return lines;
					}
		};
	}
}
