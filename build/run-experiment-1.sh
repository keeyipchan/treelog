#!/bin/sh

PROJECT_DIRECTORY=.

cd $PROJECT_DIRECTORY

clang++ -g -O0 -I $PROJECT_DIRECTORY -o bin/experiment-1 src/experiment-1.cc

chmod +x bin/experiment-1

./bin/experiment-1
