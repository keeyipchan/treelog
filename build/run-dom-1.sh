#!/bin/sh

PROJECT_DIRECTORY=.

cd $PROJECT_DIRECTORY

rm -f bin/dom-1
clang++ -g -O0 -I $PROJECT_DIRECTORY -o bin/dom-1 src/dom-1.cc

chmod +x bin/dom-1

./bin/dom-1
